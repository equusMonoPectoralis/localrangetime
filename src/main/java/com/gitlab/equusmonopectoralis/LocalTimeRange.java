package com.gitlab.equusmonopectoralis;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import lombok.Getter;
import lombok.ToString;

@ToString
public class LocalTimeRange implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    private final LocalTime start;
    @Getter
    private final LocalTime end;

    public static LocalTimeRange of(LocalTime startInclusive, LocalTime endExclusive) {
	return new LocalTimeRange(startInclusive, endExclusive);
    }

    public static LocalTimeRange ofEmpty(LocalTime time) {
	return new LocalTimeRange(time, time);
    }

    public boolean isEmpty() {
	return start.equals(end);
    }

    private LocalTimeRange(LocalTime startInclusive, LocalTime endExclusive) {
	if (endExclusive.isBefore(startInclusive)) {
	    throw new DateTimeException("La date de début doit être avant la date de fin");
	}
	this.start = startInclusive;
	this.end = endExclusive;
    }

    public boolean isConnected(LocalTimeRange other) {
	return this.equals(other) || (start.compareTo(other.getEnd()) <= 0 && other.getStart().compareTo(end) <= 0);
    }

    public LocalTimeRange intersect(LocalTimeRange other) {
	if (!isConnected(other)) {
	    return LocalTimeRange.ofEmpty(LocalTime.now());
	}
	int cmpStart = start.compareTo(other.getStart());
	int cmpEnd = end.compareTo(other.getEnd());

	if (cmpStart >= 0 && cmpEnd <= 0) {
	    return this;
	} else if (cmpStart <= 0 && cmpEnd >= 0) {
	    return other;
	} else {
	    LocalTime newStart = (cmpStart >= 0 ? start : other.getStart());
	    LocalTime newEnd = (cmpEnd <= 0 ? end : other.getEnd());
	    return LocalTimeRange.of(newStart, newEnd);
	}
    }

    public boolean isAfter(LocalTime date) {
	return start.compareTo(date) > 0;
    }

    public boolean isBefore(LocalTime date) {
	return end.compareTo(date) <= 0 && start.compareTo(date) < 0;
    }

    public boolean isAfter(LocalTimeRange other) {
	return start.compareTo(other.getEnd()) >= 0 && !other.equals(this);
    }

    public boolean isBefore(LocalTimeRange range) {
	return end.compareTo(range.getStart()) <= 0 && !range.equals(this);
    }

    public long lengthInHours() {
	return ChronoUnit.HOURS.between(start, end);
    }

    public long lengthInMinutes() {
	return ChronoUnit.MINUTES.between(start, end);
    }

    public long lengthInSeconds() {
	return ChronoUnit.SECONDS.between(start, end);
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj instanceof LocalTimeRange) {
	    LocalTimeRange other = (LocalTimeRange) obj;
	    return start.equals(other.getStart()) && end.equals(other.getEnd());
	}
	return false;
    }

    @Override
    public int hashCode() {
	return start.hashCode() ^ end.hashCode();
    }

}
